#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <vector>

using namespace std;

typedef int Mask[5][2]; // {y, x}

struct Figure{
	Mask mask;
	int width;
	int height;
};

struct Penta{
	Figure* rotations;
	int rotations_count;
	int color;
};

void apply_mask(Mask mask, char*out, int cols = 6, int offsetx = 0, int offsety = 0){
	for(int i = 0; i < 5; i++){
		out[ mask[i][0]*cols + offsety*cols + offsetx + mask[i][1] ] = '*';
	}
}

void print_penta(Mask mask){
	char out[] = "     \n     \n     \n     \n     \n";
	apply_mask(mask, out);
	printf("%s\n", out);
}

int compare_mask(Mask mask1, Mask mask2){
	int out [5][5] = {0};
	for(int i = 0; i < 5; i++){
		out[mask1[i][0]][mask1[i][1]] = 1;
	}
	for(int i = 0; i < 5; i++){
		if(!out[mask2[i][0]][mask2[i][1]]){
			return 0;
		}
	}
	return 1;
}

Figure rotate_penta(Mask mask, int matrix[2][2]){
	Figure fig = {0};
	for(int i = 0; i < 5; i++){
		fig.mask[i][0] = mask[i][0]*matrix[0][0] + mask[i][1]*matrix[0][1];
		fig.mask[i][1] = mask[i][0]*matrix[1][0] + mask[i][1]*matrix[1][1];
	}
	int x = 10, y = 10, w = -10, h = -10;
	for(int i = 0; i < 5; i++){
		if(fig.mask[i][0] > h){
			h = fig.mask[i][0];
		}
		if(fig.mask[i][0] < y){
			y = fig.mask[i][0];
		}
		if(fig.mask[i][1] > w){
			w = fig.mask[i][1];
		}
		if(fig.mask[i][1] < x){
			x = fig.mask[i][1];
		}
	}
	for(int i = 0; i < 5; i++){
		fig.mask[i][0] = fig.mask[i][0] - y;
		fig.mask[i][1] = fig.mask[i][1] - x;
	}
	fig.width = w - x + 1;
	fig.height = h - y + 1;
	return fig;
}

void fill_pentas(Penta* pentas){
	int figures [12][5][2] = {
		{{0,0}, {0,1}, {0,2}, {0,3}, {0,4}}, //1
		{{0,0}, {0,1}, {0,2}, {0,3}, {1,3}},
		{{0,0}, {0,1}, {0,2}, {0,3}, {1,2}}, //3
		{{0,0}, {0,1}, {0,2}, {1,3}, {1,2}},
		{{0,0}, {0,1}, {0,2}, {1,2}, {2,2}}, //5
		{{0,0}, {0,1}, {0,2}, {1,0}, {1,1}},
		{{0,0}, {0,1}, {0,2}, {1,0}, {1,2}}, //7
		{{0,0}, {0,1}, {1,1}, {2,1}, {2,2}},
		{{0,0}, {0,1}, {1,1}, {1,2}, {2,1}}, //9
		{{0,1}, {1,1}, {2,1}, {2,0}, {2,2}},
		{{0,0}, {0,1}, {1,1}, {1,2}, {2,2}},
		{{0,1}, {1,0}, {1,1}, {1,2}, {2,1}}, //12
	};
	for(int p = 0; p < 12; p++){
		pentas[p].color = p + 1;
		pentas[p].rotations = new Figure[8];
		int matrices[8][2][2] = {
			{{1, 0}, {0, 1}}, // normal
			{{-1, 0}, {0, -1}}, // 180 degree
			{{0, 1}, {-1, 0}}, // 90 degree
			{{0, -1}, {1, 0}}, // 270 degree
			{{1, 0}, {0, -1}}, // flip horizontal
			{{-1, 0}, {0, 1}}, // flip vertical
			{{0, 1}, {1, 0}}, // 90 degree + flip
			{{0, -1}, {-1, 0}} // 270 degree + flip
		};
		int j = 0;
		for(int i = 0; i < 8; i++){
			pentas[p].rotations[j] = rotate_penta(figures[p], matrices[i]);
			j++;
			for(int k = 0; k < j - 1; k++){
				if(compare_mask(pentas[p].rotations[k].mask, pentas[p].rotations[j - 1].mask)){
					j--;
					break;
				}
			}
		}
		pentas[p].rotations_count = j;
		if(p == 0){
			pentas[p].rotations_count = 1;
		}
	}
}


void debug_print(char*field, int width){
	for(int i = 0; i < 5; i++){
		for(int j = 0; j < width; j++){
			if(field[i*width+j]){
				printf("%x", field[i*width+j]);
			}else{
				printf("_");
			}
		}
		printf("\n");
	}
}

void place(char* field, int width, Mask mask, int x, int y, int color){
//	printf("\n(%d, %d)\n", x, y);
//	debug_print(field, width);
//	printf("\n");
	for(int i = 0; i < 5; i++){
		field[width*(mask[i][0]+y)+mask[i][1] + x] = (char)color;
	}
//	printf("\n");
//	print_penta(mask);
//	debug_print(field, width);
//	printf("\n");
}
int check_placement(char* field, int width, Mask mask, int x, int y){
	for(int i = 0; i < 5; i++){
		if(field[width*(mask[i][0]+y)+mask[i][1] + x]){
			return 0;
		}
	}
	return 1;
}

int select_penta(char*field, int width, Penta*pentas, int pentas_length, int pindex);

time_t start;

void save_solution(char*field, int width);

char**buffers;

int select_rotation(char*field, int width, Penta*pentas, int pentas_length, int pindex, int rindex){
	//print_penta(.mask);
	Figure fig = pentas[pindex].rotations[rindex];
	if(fig.width > width){
		return 0;
	}
	for(int x = 0; x < width - fig.width + 1; x++){
		for(int y = 0; y < 5 - fig.height + 1; y++){
			if(check_placement(field, width, fig.mask, x, y)){
				char* f2 = buffers[pindex];
				memcpy(f2, field, width*5+1);
				place(f2, width, fig.mask, x, y, pentas[pindex].color);
				if(pindex < pentas_length - 1){
					select_penta(f2, width, pentas, pentas_length, pindex + 1);
				}else{
					debug_print(f2, width);
					printf("%d\n\n", time(0) - start);
					save_solution(f2, width);
				}
			}
//			printf("%d, %d\n", x, y);
//			print_penta(fig.mask);
		}
	}
	return 0;
}

int select_penta(char*field, int width, Penta*pentas, int pentas_length, int pindex){
	for(int i = 0; i < pentas[pindex].rotations_count; i++){
		select_rotation(field, width, pentas, pentas_length, pindex, i);
	}
	return 0;
}

void find_solutions(int width, Penta*pentas, int pentas_length){
	char *field = new char[width*5 + 1];
	memset(field, 0, width*5+1);
	buffers = new char*[100];
	for(int i = 0; i < 100; i++){
		buffers[i] = new char[width*5 + 1];
		memset(buffers[i], 0, width*5+1);
	}

	select_penta(field, width, pentas, pentas_length, 0);

	// place 1st penta,
	// place 1st rotation of penta,
	// place 1st rotation of penta on any place
}

void html_solution(char*field, int width, FILE*f){
	fprintf(f, "<table>");
	for(int y = 0; y < 5; y++){
		fprintf(f, "<tr>");
		for(int x = 0; x < width; x++){
			fprintf(f, "<td class='b%d'>&nbsp;</td>", field[y*width + x]);
		}
		fprintf(f, "</tr>");
	}
	fprintf(f, "</table>");
}

int n = 5;
Penta* ps;

struct Solution{
	char*field;
	time_t time;
};

vector<Solution> solutions;

void save_solution(char*field, int width){

	if(field){
		Solution s;
		s.field = new char[width*5];
		memcpy(s.field, field, width*5);
		s.time = time(0);
		solutions.push_back(s);
	}

	FILE*f;
	int e = fopen_s(&f, "out.html", "wt");
	e = fprintf(f, "<html><head><link rel='stylesheet' href='out.css'/></head><body>");

	fprintf(f, "<p>%d solutions in %d seconds %s</p>", (int)solutions.size(), int(time(0) - start), (field ? "(incomplete)" : ""));

	char *pieces = new char[n*5*5];
	int offs = 0;
	for(int i = 0; i < n; i++){
		place(pieces, n*5, ps[i].rotations[0].mask, offs, 0, ps[i].color);
		offs += ps[i].rotations[0].width + 1;
	}
	html_solution(pieces, n*5, f);

	for(unsigned int i = 0; i < solutions.size(); i++){
		html_solution(solutions[i].field, width, f);
		fprintf(f, "<p>%d seconds from start</p>", solutions[i].time - start);
	}

	fprintf(f, "</html>");
	fclose(f);
}

int main(int argc, char *argv[]){
	Penta pentas[12];
	fill_pentas(pentas);
//	int psa[8] = {2,3,10,6,11,8,5,4};
	int psa[12] = {2,4,6,10,11,8,5,1,7,3,9,12};
	if(argc > 1){
		for(int i = 1; i < argc; i++){
			psa[i - 1] = atoi(argv[i]);
		}
		n = argc - 1;
	}else{
		printf("Katamino solver (solntcev@infolio.ru)\n");
		printf("Usage:\n");
		printf("\tkatamino {f1} {f2} {f3} {f4} {f5} ...\n");
		printf("\t\t{fn} pento index\n");
		printf("found solutions written in out.html\n");
		return 0;
	}
//	int psa[12] = {0};
	ps = new Penta[n];
	for(int i = 0; i < n; i++){
		ps[i] = pentas[psa[i] - 1];
	}
	start = time(0);
	find_solutions(n, ps, n);
	save_solution(NULL, n);
	/*
	for(int p = 0; p < 12; p++){
		printf("%d (%d, %d)\n", p, pentas[p].rotations[0].width, pentas[p].rotations[0].height);
		char *out = new char[71*6];
		memset(out, ' ', 71*6 - 1);
		out[71*5 - 5] = 0;
		for(int i = 0; i < 5; i++){
			out[70*i + 69] = '\n';
		}
		for(int i = 0; i < pentas[p].rotations_count; i++){
			apply_mask(pentas[p].rotations[i].mask, out, 70, i*6);
		}
		printf("%s\n", out);
//		print_penta(pentas[p].rotations[0].mask);
	}
//	*/
	return 0;
}